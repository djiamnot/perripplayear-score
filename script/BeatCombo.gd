extends VBoxContainer

onready var _beat: CheckBox = $beat
onready var _size = $size
onready var _opacity = $opacity
onready var _button = $Button
var _beat_on = false

signal beat_combo(name, beat, size, opacity)


func _ready():
	_beat.text = self.name



func _on_Button_pressed():
	emit_signal("beat_combo", self.name, _beat_on, _size.value, _opacity.value)


func _on_beat_toggled(button_pressed):
	_beat_on = button_pressed # Replace with function body.
