extends Spatial

const DEF_PORT = 9090
const PROTO_NAME = "ludus"

onready var _host_btn = $Panel/Screen/HostBar/Host
onready var _connect_btn = $Panel/Screen/ConnectionBar/Connect
onready var _disconnect_btn = $Panel/Screen/ConnectionBar/Disconnect
onready var _name_edit = $Panel/Screen/ConnectionBar/NameEdit
onready var _host_bar = $Panel/Screen/HostBar
onready var _host_edit = $Panel/Screen/ConnectionBar/Hostname
onready var _beat_combo_1 = find_node("1")
onready var _beat_combo_2 = find_node("2")
onready var _beat_combo_3 = find_node("3")
onready var _beat_combo_4 = find_node("4")
onready var _beat_combo_group = [_beat_combo_1, _beat_combo_2, _beat_combo_3, _beat_combo_4]


onready var _beats_menu:MenuButton = find_node("BeatsChoice")

onready var _score = find_node("Score")

var _beat_controls_visible = false
var hubs_visible = false

func _ready():
	#warning-ignore-all:return_value_discarded
	get_tree().connect("network_peer_disconnected", self, "_peer_disconnected")
	get_tree().connect("network_peer_connected", self, "_peer_connected")
	$AcceptDialog.get_label().align = Label.ALIGN_CENTER
	$AcceptDialog.get_label().valign = Label.VALIGN_CENTER
	_beat_combo_1.connect("beat_combo", self, "_on_beat_combo")
	_beat_combo_2.connect("beat_combo", self, "_on_beat_combo")
	_beat_combo_3.connect("beat_combo", self, "_on_beat_combo")
	_beat_combo_4.connect("beat_combo", self, "_on_beat_combo")
	# Set the player name according to the system username. Fallback to the path.
	if OS.has_environment("USERNAME"):
		_name_edit.text = OS.get_environment("USERNAME")
	else:
		var desktop_path = OS.get_system_dir(0).replace("\\", "/").split("/")
		_name_edit.text = desktop_path[desktop_path.size() - 2]

	if OS.get_environment("USERNAME") == "mis":
		print("hosting server automatically")
		_on_Host_pressed()

func _on_beat_combo(name, beat, size, opacity):
	_set_beat(name, beat, size, opacity)

func _set_beat(beat: String, on: bool, size: float, opacity: float):
	_score.set_beat(beat, on, size, opacity)

func _input(event):
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_EQUAL:
			toggle_host_visiblity()
		if event.scancode == KEY_0:
			toggle_beat_visibility()

func toggle_beat_visibility():
	_beat_controls_visible = !_beat_controls_visible
	for i in _beat_combo_group:
		i.visible = _beat_controls_visible

func toggle_host_visiblity():
	_host_bar.visible = !_host_bar.visible

func start_game():
	_host_btn.disabled = true
	_name_edit.editable = false
	_host_edit.editable = false
	_connect_btn.hide()
	_disconnect_btn.show()
	# _score.start()


func stop_game():
	_host_btn.disabled = false
	_name_edit.editable = true
	_host_edit.editable = true
	_disconnect_btn.hide()
	_connect_btn.show()
	_score.stop()


func _close_network():
	if get_tree().is_connected("server_disconnected", self, "_close_network"):
		get_tree().disconnect("server_disconnected", self, "_close_network")
	if get_tree().is_connected("connection_failed", self, "_close_network"):
		get_tree().disconnect("connection_failed", self, "_close_network")
	if get_tree().is_connected("connected_to_server", self, "_connected"):
		get_tree().disconnect("connected_to_server", self, "_connected")
	stop_game()
	$AcceptDialog.show_modal()
	$AcceptDialog.get_close_button().grab_focus()
	get_tree().set_network_peer(null)


func _connected():
	print("player connected")
	_score.rpc("set_player_name", _name_edit.text)


func _peer_connected(id):
	print("peer connected")
	_score.on_peer_add(id)


func _peer_disconnected(id):
	print("peer disconnected ")
	_score.on_peer_del(id)


func _on_Host_pressed():
	var host = WebSocketServer.new()
	host.listen(DEF_PORT, PoolStringArray(["ludus"]), true)
	get_tree().connect("server_disconnected", self, "_close_network")
	get_tree().set_network_peer(host)
	_score.add_player(1, _name_edit.text)
	start_game()


func _on_Disconnect_pressed():
	_close_network()

func _on_Connect_pressed():
	var host = WebSocketClient.new()
	host.connect_to_url("ws://" + _host_edit.text + ":" + str(DEF_PORT), PoolStringArray([PROTO_NAME]), true)
	get_tree().connect("connection_failed", self, "_close_network")
	get_tree().connect("connected_to_server", self, "_connected")
	get_tree().set_network_peer(host)
	start_game()

func _on_Notes_pressed():
	_score.on_notes_pressed()

func _on_beat1_toggled(button_pressed):
	var opacity = $Panel/Screen/HostBar/Beats/beat1/opacity.value
	var size = $Panel/Screen/HostBar/Beats/beat1/size.value
	_score.set_beat(1, button_pressed, size, opacity)

func _on_Beatless_pressed():
	_score.make_adlib()


func _on_Beatless_toggled(button_pressed):
	if button_pressed:
		_score.make_adlib()
	else:
		_score.remove_adlib()


func _on_RandomBeats_pressed():
	var on = [true, false]
	for i in range(4):
		_score.set_beat(str(i+1), on[randi()%on.size()], randf()*100, randf()*100)

func _on_Hubs_toggled(button_pressed):
	if button_pressed:
		hubs_visible = true
	else:
		hubs_visible = false
	_score.hubs_on(hubs_visible)




func _on_ResetNotes_pressed():
	_score.on_reset()
