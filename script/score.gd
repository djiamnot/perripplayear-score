extends Control

const _crown = preload("res://img/crown.png")

onready var _list = find_node("PlayersList")
# onready var _action = $HBoxContainer/VBoxContainer/Action
onready var _score_line = find_node("ScoreLine")
onready var _score_heading = find_node("ScoreHeading")
onready var _beat_frames = find_node("BeatFrames")
onready var _beat_textures = find_node("BeatTextures")
onready var _beats_box = $VBoxContainer/ScoreBeatContainer/ScoreMeasure/BeatsBox
onready var duck = $VBoxContainer/ScoreHeading/Duck
onready var debug = find_node("debug")

onready var _beat_frame = load("res://img/score/beat_frame.png")
onready var _beat_texture = load("res://img/score/beat_texture.png")
onready var _beat_transparent = load("res://img/score/transparent.png")

export (Color) var _staff_target_color

var _staff_group1: Array = ["res://img/score/white/group1/00_1.png","res://img/score/white/group1/01_1.png","res://img/score/white/group1/02_1.png","res://img/score/white/group1/03_2.png","res://img/score/white/group1/04_1.png","res://img/score/white/group1/05_2.png","res://img/score/white/group1/06_1.png","res://img/score/white/group1/07_2.png","res://img/score/white/group1/08_1.png","res://img/score/white/group1/09_2.png","res://img/score/white/group1/10_1.png"]
var _staff_group2: Array = ["res://img/score/white/group2/00_2.png","res://img/score/white/group2/01_2.png","res://img/score/white/group2/02_2.png","res://img/score/white/group2/03_1.png","res://img/score/white/group2/04_2.png","res://img/score/white/group2/05_1.png","res://img/score/white/group2/06_2.png","res://img/score/white/group2/07_1.png","res://img/score/white/group2/08_2.png","res://img/score/white/group2/09_1.png","res://img/score/white/group2/10_2.png"]

var _staff_textures_group1: Array = []
var _staff_textures_group2: Array = []

var _players = []
var _beatless_textures = []
export (AtlasTexture) var _score_notes
export (Texture) var _heading_texture
var _players_group1 = []
var _players_group2 = []
var _group1_notes_variant = 0
var _group2_notes_variant = 1
var time_now
var time_elapsed = 0
var reports_per_second = 2
var report_time = 1.0 / reports_per_second
var _staff_modulate = Color("#a8e3e7")
var _current_staff_line = 0

var hubs_off_color = Color(0.286275, 0.321569, 0.588235, 0.039216)
var hubs_on_color = Color(1, 1, 1, 1)


func _ready():
	preload_textures()
	_score_line.modulate = _staff_modulate
	duck.modulate = hubs_off_color


func _process(delta):
	time_elapsed += delta
	if time_elapsed >= report_time:
		get_current_time()
		time_elapsed = 0
	if _score_line.modulate.g < _staff_modulate.g:
		rpc("_fade_staff_color", delta)

sync func _fade_staff_color(delta):
	if _score_line.modulate.g < _staff_modulate.g:
		_score_line.modulate.g += delta * 0.05

func preload_textures():
	for tr in _staff_group1:
		var res = load(tr)
		_staff_textures_group1.append(res)
	for tr in _staff_group2:
		var res = load(tr)
		_staff_textures_group2.append(res)


master func set_player_name(name):
	var sender = get_tree().get_rpc_sender_id()
	rpc("update_player_name", sender, name)


sync func update_player_name(player, name):
	var pos = _players.find(player)
	if pos != -1:
		_list.set_item_text(pos, name)

func pp_time():
	return "%s:%s:%s" % [time_now.hour, time_now.minute, time_now.second]


sync func del_player(id):
	var pos = _players.find(id)
	if pos == -1:
		return
	_players.remove(pos)
	_list.remove_item(pos)
	del_player_from_group(id)
	print("groups:", _players_group1, ", ", _players_group2)

func del_player_from_group(id):
	var grp
	var pos
	for group in [_players_group1, _players_group2]:
		pos = group.find(id)
		if pos > -1:
			grp = group
		else:
			return
	grp.remove(pos)



sync func add_player(id, name=""):
	_players.append(id)
	if randi() % 2 == 0:
		_players_group1.append(id)
	else:
		_players_group2.append(id)
	if name == "":
		_list.add_item("... connecting ...", null, false)
	else:
		_list.add_item(name, null, false)


func get_player_name(pos):
	if pos < _list.get_item_count():
		return _list.get_item_text(pos)
	else:
		return "Error!"

func stop():
	_players.clear()
	_list.clear()


func on_peer_add(id):
	if not get_tree().is_network_server():
		return
	for i in range(0, _players.size()):
		rpc_id(id, "add_player", _players[i], get_player_name(i))
	rpc("add_player", id)

func on_peer_del(id):
	if not get_tree().is_network_server():
		return
	rpc("del_player", id)


sync func _log(what):
	$RichTextLabel.add_text(what + "\n")


func on_notes_pressed():
	assign_textures()
	for i in _players_group1:
		rpc_id(i, "_set_notes_variant", 1, _current_staff_line)
	for i in _players_group2:
		rpc_id(i, "_set_notes_variant", 2, _current_staff_line)

func on_reset():
	_current_staff_line = -1
	on_notes_pressed()

func assign_textures():
	if _current_staff_line < _staff_textures_group1.size() - 1:
		_current_staff_line += 1
	else:
		_current_staff_line = 0

sync func _set_notes_variant(group, current_tex_idx):
	if group == 1:
		_score_line.texture = _staff_textures_group1[current_tex_idx]
	if group == 2:
		_score_line.texture = _staff_textures_group2[current_tex_idx]
	_score_line.modulate.g = 0.5
	debug.text = "%s %s" % [group, current_tex_idx]

func even(num):
	if num % 2 == 0:
		return true
	else:
		return false

func get_current_time():
	time_now = OS.get_datetime()
	return time_now

func set_beat(num: String, on: bool, size: float, opacity: float):
	rpc("_set_beat", num, on, size, opacity)

sync func _set_beat(beat : String, on : bool, size : float, opacity : float):
	print(beat, " ", on, " ", size, " ", opacity)
	var beat_pos = _beat_frames.get_node("frame"+beat)
	var beat_tex = _beat_textures.get_node("texture"+beat)
	if on:
		beat_pos.texture = _beat_frame
		beat_pos.rect_scale[0] = size * 0.01
		beat_tex.texture = _beat_texture
		beat_tex.rect_scale[0] = size * 0.01
		beat_tex.modulate = Color(1, 1, 1, opacity * 0.01)
	else:
		beat_pos.texture = _beat_transparent
		beat_tex.texture = _beat_transparent

func make_adlib():
	rpc("_make_adlib")

sync func _make_adlib():
	for i in range(4):
		rpc("_set_beat", str(i+1), false, 0, 0)
	for i in range(randi()%60):
		var texi = TextureRect.new()
		texi.texture = _beat_texture
		texi.rect_position = Vector2(randi()%int(_beats_box.rect_size.x-100), randi()%int(_beats_box.rect_size.y)*0.5)
		texi.rect_scale = Vector2(randf() * 0.6, randf() * 0.5)
		texi.modulate = Color(1, 1, 1, randf())
		_beats_box.add_child(texi)

func remove_adlib():
	rpc("_remove_adlib")

sync func _remove_adlib():
	for i in _beats_box.get_children():
		i.queue_free()

func hubs_on(on: bool):
	rpc("_hubs_on", on)

sync func _hubs_on(on: bool):
	if on:
		duck.modulate = hubs_on_color
	else:
		duck.modulate = hubs_off_color
